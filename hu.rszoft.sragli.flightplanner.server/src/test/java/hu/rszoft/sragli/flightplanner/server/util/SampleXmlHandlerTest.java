package hu.rszoft.sragli.flightplanner.server.util;

import hu.rszoft.sragli.flightplanner.server.entity.City;
import hu.rszoft.sragli.flightplanner.server.entity.Flight;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SampleXmlHandlerTest {

    private String[] airlines = new String[] { "Lufthansa", "TWA", "Delta" };
    private String[] cities = new String[] { "Los Angeles", "New York", "Paris", "Hamburg", "Budapest",
            "Kuala Lumpur" };

    @Before
    public void init() throws IOException {
        Instant current = Instant.now();
        Instant next = null;
        Random rnd = new Random();
        List<Flight> timetable = new ArrayList<>();
        for (int a = 0; a < airlines.length; a++) {
            for (int i = 0; i < cities.length - 1; i++) {
                City c1 = new City(cities[i], rnd.nextInt(1_000_000) + 5000);
                City c2 = new City(cities[i + 1], rnd.nextInt(1_000_000) + 5000);
                next = current.plus(Duration.ofHours(rnd.nextInt(8) + 1));
                Flight flight = new Flight(airlines[a], c1, c2,
                        current.toEpochMilli(), next.toEpochMilli());
                timetable.add(flight);
                current = next.plus(Duration.ofHours(1));
            }
        }
        SampleXmlHandler.write(timetable);
    }

    @Test
    public void shouldExistXmlFile() {
        File xml = new File("sample_timetable.xml");
        Assert.assertTrue(xml.exists());
    }

    @Test(expected = Test.None.class)
    public void shouldReadXmlFile() throws IOException {
        List<Flight> timetable = SampleXmlHandler.read();
        Assert.assertFalse(timetable.isEmpty());
    }

}
