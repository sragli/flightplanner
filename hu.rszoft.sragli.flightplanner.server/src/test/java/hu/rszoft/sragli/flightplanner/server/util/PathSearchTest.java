package hu.rszoft.sragli.flightplanner.server.util;

import hu.rszoft.sragli.flightplanner.server.entity.City;
import hu.rszoft.sragli.flightplanner.server.entity.Flight;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class PathSearchTest {

    @Test(expected = Test.None.class)
    public void shouldPathPresent() throws IOException {
        List<Flight> timetable = SampleXmlHandler.read();
        Assert.assertFalse(timetable.isEmpty());

        PathSearch pathSearch = new PathSearch(timetable);
        City start = timetable.get(0).getStart();
        City end = timetable.get(timetable.size() - 1).getDestination();
        Assert.assertFalse(pathSearch.getPath(start, end).isEmpty());
    }

}
