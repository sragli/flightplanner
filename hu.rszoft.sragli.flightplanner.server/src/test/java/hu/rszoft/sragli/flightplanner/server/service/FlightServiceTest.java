package hu.rszoft.sragli.flightplanner.server.service;

import org.junit.Assert;
import org.junit.Test;

public class FlightServiceTest {

    @Test(expected = Test.None.class)
    public void shouldAirlinesPresent() {
        FlightService flightService = new FlightService();
        Assert.assertFalse(flightService.listAirlines().isEmpty());
    }

}
