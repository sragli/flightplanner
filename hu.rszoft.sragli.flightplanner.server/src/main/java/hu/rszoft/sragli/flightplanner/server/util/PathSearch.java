package hu.rszoft.sragli.flightplanner.server.util;

import hu.rszoft.sragli.flightplanner.server.entity.City;
import hu.rszoft.sragli.flightplanner.server.entity.Flight;

import java.time.Duration;
import java.util.*;

public class PathSearch {

    private Set<Flight> unSettledFlights;

    public PathSearch(List<Flight> flights) {
        this.unSettledFlights = new HashSet<>(flights);
    }

    public LinkedList<Flight> getPath(City start, City destination) {
        Flight startFlight = unSettledFlights.stream()
                .filter(flight -> flight.getStart().getName().equals(start.getName())).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Start city " + start.getName() + " is not found in "
                                                                + "timetable"));

        LinkedList<Flight> path = new LinkedList<>();
        path.add(startFlight);
        unSettledFlights.remove(startFlight);

        Flight currentFlight = startFlight;
        while (!unSettledFlights.isEmpty()) {
            Flight nextFlight = getNextFlight(currentFlight);
            if (nextFlight != null) {
                path.add(nextFlight);
                if (nextFlight.getDestination().getName().equals(destination.getName())) {
                    break;
                }
                unSettledFlights.remove(nextFlight);
                currentFlight = nextFlight;
            } else {
                return new LinkedList<>();
            }
        }

        return path;
    }

    private Flight getNextFlight(Flight current) {
        return unSettledFlights.stream()
                .filter(flight -> flight.getStart().getName().equals(current.getDestination().getName()))
                .filter(flight -> flight.getDeparture() > current.getArrival())
                .min(Comparator.comparingLong(flight -> flight.calculateDuration().toMillis()))
                .orElse(null);
    }

}
