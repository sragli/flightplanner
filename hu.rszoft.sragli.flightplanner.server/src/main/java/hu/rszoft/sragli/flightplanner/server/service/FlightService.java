package hu.rszoft.sragli.flightplanner.server.service;

import hu.rszoft.sragli.flightplanner.server.entity.City;
import hu.rszoft.sragli.flightplanner.server.entity.Flight;
import hu.rszoft.sragli.flightplanner.server.util.PathSearch;
import hu.rszoft.sragli.flightplanner.server.util.SampleXmlHandler;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Log
@Service
public class FlightService {

    private List<Flight> timetable;

    private City start;

    private City destination;

    public FlightService() {
        try {
            timetable = SampleXmlHandler.read();
            start = getSmallestDeparture();
            destination = getLargestDestination();
        } catch (IOException e) {
            log.severe(e.toString());
            // fallback
            timetable = new ArrayList<>();
        }
    }

    public List<Flight> getShortestPathByAirline(String airline) {
        PathSearch pathSearch = new PathSearch(listFlightsByAirline(airline));
        return pathSearch.getPath(start, destination);
    }

    public List<Flight> listShortestPaths() {
        PathSearch pathSearch = new PathSearch(timetable);
        return pathSearch.getPath(start, destination);
    }

    public City getSmallestDeparture() {
        return start == null ?
                timetable.stream().map(Flight::getStart)
                        .min(Comparator.comparingInt(City::getPopulation))
                        .orElseThrow(() -> new RuntimeException("Empty timetable"))
                : start;
    }

    public City getLargestDestination() {
        return destination == null ?
                timetable.stream().map(Flight::getDestination)
                        .max(Comparator.comparingInt(City::getPopulation))
                        .orElseThrow(() -> new RuntimeException("Empty timetable"))
                : destination;
    }

    public Set<String> listAirlines() {
        return timetable.stream().map(Flight::getAirline).collect(Collectors.toSet());
    }

    private List<Flight> listFlightsByAirline(String airline) {
        return timetable.stream().filter(entry -> entry.getAirline().equals(airline))
                .collect(Collectors.toList());
    }

    private List<Flight> listFlightsByDepartureAndDestination(City start, City destination) {
        return timetable.stream()
                .filter(flight -> start == null || flight.getStart() == start)
                .filter(flight -> destination == null || flight.getDestination() == destination)
                .collect(Collectors.toList());
    }

}
