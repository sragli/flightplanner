package hu.rszoft.sragli.flightplanner.server.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class City {

    private String name;

    private Integer population;

    @Override
    public String toString() {
        return name + ", " + population + " lakos";
    }

}
