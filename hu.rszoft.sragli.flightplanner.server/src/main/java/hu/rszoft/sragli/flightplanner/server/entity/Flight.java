package hu.rszoft.sragli.flightplanner.server.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.Duration;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Flight {

    private String airline;

    private City start;

    private City destination;

    private Long departure;

    private Long arrival;

    public Duration calculateDuration() {
        return Duration.ofMillis(arrival - departure);
    }

    @Override
    public String toString() {
        Duration duration = calculateDuration();
        return start.getName() + " -> " + destination.getName()
               + ": " + duration.toHoursPart() + " óra " + duration.toMinutesPart() + " perc";
    }

}
