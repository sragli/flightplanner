package hu.rszoft.sragli.flightplanner.server.controller;

import hu.rszoft.sragli.flightplanner.server.entity.City;
import hu.rszoft.sragli.flightplanner.server.entity.Flight;
import hu.rszoft.sragli.flightplanner.server.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;
import java.util.List;

@RestController
public class FlightController {

	private final FlightService flightService;

	@Autowired
	public FlightController(FlightService flightService) {
		this.flightService = flightService;
	}

	@GetMapping("/check")
	public String check() {
		return "OK";
	}

	@GetMapping("/listShortestPaths")
	public String listShortestPaths() {
		StringWriter writer = new StringWriter();
		PrintWriter result = new PrintWriter(writer);

		City departure = flightService.getSmallestDeparture();
		City destination = flightService.getLargestDestination();
		result.println("A legkisebb város: " + departure);
		result.println("A legnagyobb város: " + destination);
		result.println();

		printShortestPathsByAirline(result);
        printShortestPath(result);

		return writer.toString();
	}

    private void printShortestPath(PrintWriter result) {
	    result.println("Bármely légitársasággal a legrövidebb út:");
        List<Flight> paths = flightService.listShortestPaths();
	    if (paths.isEmpty()) {
		    result.println("    Nincs útvonal");
	    } else {
		    Duration sum = Duration.ofMillis(0L);
		    for (Flight entry : paths) {
			    sum = sum.plus(entry.calculateDuration());
			    result.println("    " + entry.getAirline() + ": " + entry);
		    }
		    result.println("    -------");
		    result.println("    Összesen: " + sum.toHoursPart() + " óra " + sum.toMinutesPart() + " perc");
	    }
    }

    private void printShortestPathsByAirline(PrintWriter result) {
		result.println("A legrövidebb út:");
		for (String airline : flightService.listAirlines()) {
			result.println("    " + airline + ":");

			List<Flight> paths = flightService.getShortestPathByAirline(airline);
			if (paths.isEmpty()) {
				result.println("        Nincs útvonal");
			} else {
				Duration sum = Duration.ofMillis(0L);
				for (Flight entry : paths) {
					sum = sum.plus(entry.calculateDuration());
					result.println("        " + entry);
				}
				result.println("        -------");
				result.println("        Összesen: " + sum.toHoursPart() + " óra " + sum.toMinutesPart() + " perc");
				result.println();
			}
		}
	}

}
