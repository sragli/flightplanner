package hu.rszoft.sragli.flightplanner.server.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import hu.rszoft.sragli.flightplanner.server.entity.Flight;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class SampleXmlHandler {

    public static void write(List<Flight> timetable) throws IOException {
        try (FileWriter writer = new FileWriter("sample_timetable.xml")) {
            XmlMapper mapper = new XmlMapper();
            String xml = mapper.writeValueAsString(timetable);
            writer.write(xml);
        }
    }

    public static List<Flight> read() throws IOException {
        List<Flight> timetable = null;
        try (FileReader reader = new FileReader("sample_timetable.xml")) {
            XmlMapper mapper = new XmlMapper();
            timetable = mapper.readValue(reader, new TypeReference<List<Flight>>() {});
        }
        return timetable;
    }

}
