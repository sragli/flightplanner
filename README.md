# Summary

A microservices-based prototype application to display flight timetables.

# Usage

* Check out the repository
* Start the server: cd hu.rszoft.sragli.flightplanner.server; mvn spring-boot:run
* Start the client: cd hu.rszoft.sragli.flightplanner.client; mvn spring-boot:run
* Enter the following URL to a web browser: http://localhost:9080/list_shortest_paths
	* username: user
	* password: see in the log of client start, look for "Using generated security password"

# Requirements

* Java Runtime 9 or newer
* Maven