package hu.rszoft.sragli.flightplanner.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class FlightWebClientController {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/check")
	public String handleCheckRequest() {
		return this.restTemplate.getForObject("http://flight-service/check", String.class);
	}

	@GetMapping(value = "/list_shortest_paths", produces = "text/plain")
	public String handleShortestPathsRequest() {
		return this.restTemplate.getForObject("http://flight-service/listShortestPaths", String.class);
	}

}