* New path search algorithm
    * Ant colony shortest path optimization
    * B-tree
    * Dijkstra's algorithm
    * A*

* Better input generation
* HTML templates
